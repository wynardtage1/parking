from flask_sqlalchemy import SQLAlchemy
from pydantic import BaseModel, ConfigDict, model_validator
from sqlalchemy import VARCHAR, Boolean, Column, DateTime, Float, ForeignKey, Integer
from sqlalchemy.orm import relationship

db = SQLAlchemy()


class PlacesValidation(BaseException):
    pass


class Client(db.Model):
    __tablename__ = "client"
    id = Column(Integer, primary_key=True)
    name = Column(VARCHAR(50), nullable=False)
    surname = Column(VARCHAR(50), nullable=False)
    credit_card = Column(VARCHAR(50))
    car_number = Column(VARCHAR(10))
    debt = Column(Float, default=0.0)

    client_parking = relationship("ClientParking", cascade="delete, save-update, merge")

    class ApiSchema(BaseModel):
        model_config = ConfigDict(extra="forbid")
        name: str
        surname: str
        credit_card: str = None
        car_number: str = None

    def as_dict_(self):
        return {
            col.name: getattr(self, col.name)
            for col in self.__table__.columns
            if getattr(self, col.name) is not None
        }


class Parking(db.Model):
    __tablename__ = "parking"
    id = Column(Integer, primary_key=True)
    address = Column(VARCHAR(100))
    opened = Column(Boolean)
    cost_per_hour = Column(Float, nullable=False)
    count_places = Column(Integer, nullable=False)
    count_available_places = Column(Integer, nullable=False)

    class ApiSchema(BaseModel):
        model_config = ConfigDict(extra="forbid")
        address: str = None
        opened: bool = None
        cost_per_hour: float
        count_places: int
        count_available_places: int

        @model_validator(mode="after")
        def validate_places(self):
            if self.count_places < self.count_available_places:
                raise TypeError
            return self

    def as_dict_(self):
        return {
            col.name: getattr(self, col.name)
            for col in self.__table__.columns
            if getattr(self, col.name) is not None
        }

    @property
    def cost_per_second(self):
        return self.cost_per_hour / 3600


class BarrierOpening(db.Model):
    __tablename__ = "barrier"
    parking_id = Column(Integer, ForeignKey(Parking.id), primary_key=True)
    open_time = Column(Float, default=0.0)


class ClientParking(db.Model):
    __tablename__ = "client_parking"
    id = Column(Integer, primary_key=True)
    client_id = Column(Integer, ForeignKey(Client.id))
    parking_id = Column(Integer, ForeignKey(Parking.id))
    time_in = Column(DateTime)
    time_out = Column(DateTime)

    class ApiSchema(BaseModel):
        model_config = ConfigDict(extra="forbid")
        client_id: int
        parking_id: int
