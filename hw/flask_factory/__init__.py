import os

from flask import Flask

from hw.flask_factory.api import api
from hw.flask_factory.models import db
from hw.flask_factory.views import views


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SQLALCHEMY_DATABASE_URI="sqlite:///parking.db",
        SQLALCHEMY_TRACK_MODIFICATIONS=False,
        DEBUG=True,
    )
    app.register_blueprint(api)
    app.register_blueprint(views)

    if test_config is None:
        app.config.from_pyfile("config.py", silent=True)
    else:
        app.config.from_mapping(**test_config)

    db.init_app(app)
    with app.app_context():
        # db.drop_all()
        db.create_all()

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    return app
