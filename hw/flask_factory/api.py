import logging

from flask import Blueprint, jsonify, request
from pydantic import ValidationError
from werkzeug.exceptions import BadRequest, UnsupportedMediaType

from hw.flask_factory.models import BarrierOpening, Client, ClientParking, Parking, db
from hw.flask_factory.queries import (
    add_model,
    check_in,
    check_out,
    delete_client,
    get_client,
    get_parking,
    is_execute_barrier,
    is_exists,
    is_full,
    is_inside,
    is_inside_in_concrete_parking,
)

exc_collector = logging.getLogger("exception_collector")
api = Blueprint("api", __name__)


@api.route("/clients")
def get_all_clients():
    # try:
    return jsonify({"clients": [i.as_dict_() for i in db.session.query(Client).all()]})
    # except Exception as e:
    #     exc_collector.info(f"{type(e)}\n{e}\n\n")
    #     return jsonify({"Error": "Internal server error"}), 500


@api.route("/clients/<int:client_id>", methods=["GET", "DELETE"])
def get_client_by_id(client_id):
    # try:
    session = db.session
    client = get_client(session, client_id)
    if not client:
        return jsonify({"Error": "Client not found."}), 404
    if request.method == "GET":
        return jsonify({"Client": client.as_dict_()})
    else:
        delete_client(session, client_id)
        return jsonify({"Info": "Client account deleted."})
    # except Exception as e:
    #     exc_collector.info(f"{type(e)}\n{e}\n\n")
    #     return jsonify({"Error": "Internal server error"}), 500


@api.route("/clients", methods=["POST"])
@api.route("/parking", methods=["POST"])
def load_item():
    # try:
    try:
        data = request.json
    except UnsupportedMediaType as e:
        return jsonify({"Error": f"{e}"}), 415
    except BadRequest as e:
        return jsonify({"Error": f"{e}"}), 400

    if str(request.url_rule) == "/clients":
        model = Client
    else:
        model = Parking

    try:
        model = model(**model.ApiSchema(**data).model_dump())
    except ValidationError:
        return (
            jsonify(
                {
                    "Error": "Validation error",
                    "Expected": model.ApiSchema.model_json_schema(mode="serialization"),
                }
            ),
            400,
        )
    except TypeError:
        return (
            jsonify(
                {
                    "Error": "Validation error",
                    "Info": "Count_places must be more or equal count_available_places",
                }
            ),
            400,
        )

    session = db.session

    add_model(session, model)
    if isinstance(model, Parking):
        if model.opened is not None:
            add_model(session, BarrierOpening(parking_id=model.id))
        return jsonify({"Info": "Parking added"}), 201
    return jsonify({"Info": "Client account added"}), 201

    # except Exception as e:
    #     exc_collector.info(f"{type(e)}\n{e}\n\n")
    #     return jsonify({"Error": "Internal server error"}), 500


@api.route("/client_parking", methods=["POST", "DELETE"])
def check_in_check_out():
    # try:
    try:
        data = request.json
    except UnsupportedMediaType as e:
        return jsonify({"Error": f"{e}"}), 415
    except BadRequest as e:
        return jsonify({"Error": f"{e}"}), 400

    try:
        ClientParking.ApiSchema(**data)
    except ValidationError:
        return (
            jsonify(
                {
                    "Error": "Validation error",
                    "Expected": ClientParking.ApiSchema.model_json_schema(
                        mode="serialization"
                    ),
                }
            ),
            400,
        )
    session = db.session

    parking_id, client_id = data["parking_id"], data["client_id"]
    if not is_exists(session, parking_id, client_id):
        return jsonify({"Error": "Parking or client not found."}), 404

    if is_execute_barrier(session, parking_id):
        return jsonify({"Error": "Barrier executing, get in line."}), 409

    if request.method == "POST":
        if is_inside_in_concrete_parking(session, parking_id, client_id):
            return (
                jsonify(
                    {"Error": f"Client #{client_id} inside parking #{parking_id}."}
                ),
                409,
            )
        if is_inside(session, client_id):
            return (
                jsonify(
                    {
                        "Error": f"Client #{client_id} inside some parking (The camera detected the perpetrator)"
                    }
                ),
                409,
            )
        if is_full(session, parking_id):
            return jsonify({"Error": "Sorry, parking is full"}), 409
        check_in(session, parking_id, client_id)
    else:
        if not is_inside_in_concrete_parking(session, parking_id, client_id):
            return (
                jsonify(
                    {"Error": f"Client #{client_id} not in parking #{parking_id}."}
                ),
                409,
            )
        if not is_inside(session, client_id):
            return (
                jsonify(
                    {
                        "Error": f"Client #{client_id} not inside some parking (The camera detected the bandit)"
                    }
                ),
                409,
            )
        check_out(session, parking_id, client_id)

    if get_parking(session, parking_id).opened is not None:
        return jsonify({"Info": "The barrier is open for 60 seconds"}), (
            201 if request.method == "POST" else 200
        )

    return jsonify({"Info": "You may pass"}), (201 if request.method == "POST" else 200)

    # except Exception as e:
    #     exc_collector.info(f"{type(e)}\n{e}\n\n")
    #     return jsonify({"Error": "Internal server error"}), 500
