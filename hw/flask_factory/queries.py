from datetime import datetime
from time import time
from typing import Optional

from flask_sqlalchemy.session import Session
from sqlalchemy import and_

from hw.flask_factory.models import BarrierOpening, Client, ClientParking, Parking


def add_model(session: Session, model):
    session.add(model)
    session.commit()


def add_models(session: Session, models):
    session.add_all(models)
    session.commit()


def get_parking(session: Session, parking_id: int):
    return session.get(Parking, parking_id)


def get_client(session: Session, client_id: int):
    return session.get(Client, client_id)


def delete_client(session: Session, client_id: int):
    session.delete(get_client(session, client_id))
    session.commit()


def is_exists(session: Session, parking_id: int, client_id: int) -> Optional[bool]:
    return get_parking(session, parking_id) and get_client(session, client_id)


def is_inside_in_concrete_parking(session: Session, parking_id: int, client_id: int):
    return (
        session.query(ClientParking)
        .filter(
            and_(
                ClientParking.client_id == client_id,
                ClientParking.parking_id == parking_id,
                ClientParking.time_out.is_(None),
            )
        )
        .scalar()
    )


def is_inside(session: Session, client_id: int):
    return (
        session.query(ClientParking)
        .filter(
            and_(ClientParking.client_id == client_id, ClientParking.time_out.is_(None))
        )
        .scalar()
    )


def is_full(session: Session, parking_id: int):
    return get_parking(session, parking_id).count_available_places <= 0


def is_execute_barrier(session: Session, parking_id: int):
    if get_parking(session, parking_id).opened is not None:
        open_time = (
            session.query(BarrierOpening.open_time)
            .where(BarrierOpening.parking_id == parking_id)
            .scalar()
        )
        if time() - open_time < 60:
            close_barrier(session, parking_id)
            return True
    return False


def open_barrier(session: Session, parking_id: int):
    parking = get_parking(session, parking_id)
    if parking.opened is not None:
        update_time_point_barrier(session, parking_id)
        parking.opened = True
    session.commit()


def update_time_point_barrier(session: Session, parking_id: int):
    session.query(BarrierOpening).where(BarrierOpening.parking_id == parking_id).update(
        {BarrierOpening.open_time: time()}
    )


def close_barrier(session: Session, parking_id: int):
    parking = get_parking(session, parking_id)
    if parking.opened:
        parking.opened = False
    session.commit()


def insert_client_parking(session: Session, parking_id: int, client_id: int):
    session.add(
        ClientParking(
            parking_id=parking_id, client_id=client_id, time_in=datetime.now()
        )
    )
    session.commit()


def check_in(session: Session, parking_id: int, client_id: int):
    insert_client_parking(session, parking_id, client_id)
    parking = get_parking(session, parking_id)
    parking.count_available_places -= 1
    session.commit()
    if parking.opened is not None:
        open_barrier(session, parking_id)


def calculate_client(session, parking_id: int, client_id: int, now):
    time_in = (
        session.query(ClientParking.time_in)
        .filter(
            and_(
                ClientParking.client_id == client_id,
                ClientParking.parking_id == parking_id,
                ClientParking.time_out.is_(None),
            )
        )
        .scalar()
    )
    if time_in:
        return round(
            (now - time_in).total_seconds()
            * get_parking(session, parking_id).cost_per_second,
            2,
        )
    return None


def update_client_parking(session: Session, parking_id: int, client_id: int):
    client = get_client(session, client_id)
    now = datetime.now()
    pay = calculate_client(session, parking_id, client_id, now)
    if pay is None:
        return False
    if client.credit_card:
        debit(client.credit_card, pay)
    else:
        client.debt += pay

    session.query(ClientParking).filter(
        and_(
            ClientParking.parking_id == parking_id,
            ClientParking.client_id == client_id,
            ClientParking.time_out.is_(None),
        )
    ).update({ClientParking.time_out: now})
    session.commit()
    return True


def check_out(session: Session, parking_id: int, client_id: int):
    update_client_parking(session, parking_id, client_id)
    parking = get_parking(session, parking_id)
    parking.count_available_places += 1
    session.commit()
    if parking.opened is not None:
        open_barrier(session, parking_id)


def debit(credit_card, pay): ...
