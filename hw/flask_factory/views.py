from flask import Blueprint, Response, send_from_directory

views = Blueprint("views", __name__)


@views.route("/load_favicon")
def load_favicon():
    return send_from_directory(".", "favicon.ico", mimetype="image/x-icon")


@views.route("/get_favicon")
def set_favicon():
    return send_from_directory(".", "hello.html", mimetype="text/html")


@views.after_request
def set_headers(response: Response):
    response.headers["Cache-Control"] = "max-age=3600"
    return response
