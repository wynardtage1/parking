from time import sleep

import pytest

from hw.flask_factory.queries import get_client, get_parking, is_execute_barrier
from hw.test_app.data_factories import ClientFactory, ParkingFactory


@pytest.mark.parametrize("rule", ("/clients", "/clients/1"))
def test_gets(client, rule):
    assert client.get(rule).status_code == 200


def test_add_client(client):
    assert client.post("/clients", json={"name": "Nada", "surname": "Rifki"}).json == {
        "Info": "Client account added"
    }


def test_add_parking(client):
    assert client.post(
        "/parking",
        json={"cost_per_hour": 15, "count_places": 10, "count_available_places": 5},
    ).json == {"Info": "Parking added"}


@pytest.mark.parking
def test_check_in(client, session):
    client_id = 1
    parking_id = 1
    before_count_available_places = get_parking(
        session, parking_id
    ).count_available_places
    assert client.post(
        "/client_parking",
        json={
            "client_id": client_id,
            "parking_id": parking_id,
        },
    ).json == {"Info": "The barrier is open for 60 seconds"}

    assert is_execute_barrier(session, parking_id)
    assert (
        get_parking(session, parking_id).count_available_places + 1
        == before_count_available_places
    )


@pytest.mark.parking
def test_check_out(client, session):
    sleep(60)
    client_id = 1
    parking_id = 1
    parking = get_parking(session, parking_id)
    before_count_available_places = parking.count_available_places
    assert client.delete(
        "/client_parking",
        json={
            "client_id": client_id,
            "parking_id": parking_id,
        },
    ).json == {"Info": "The barrier is open for 60 seconds"}
    assert parking.count_available_places - 1 == before_count_available_places


def test_add_client_by_factory(client, session):
    ClientFactory()
    assert get_client(session, 3)
    # assert client.post('/clients', json={
    #     'name': client_.name,
    #     'surname': client_.surname,
    #     'credit_card': client_.credit_card,
    #     'car_number': client_.car_number
    # }).json == {
    #     'Info': "Client account added"
    # }


def test_add_parking_by_factory(client, session):
    ParkingFactory()
    assert get_parking(session, 3)
    # assert client.post('/parking', json={
    #     'address': parking.address,
    #     'opened': parking.opened,
    #     'cost_per_hour': parking.cost_per_hour,
    #     'count_places': parking.count_places,
    #     "count_available_places": parking.count_available_places
    # }).json == {
    #     'Info': "Parking added"
    # }
