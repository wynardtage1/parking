import string
from random import choice, randint

from factory import Faker, LazyAttribute
from factory.alchemy import SQLAlchemyModelFactory
from factory.fuzzy import FuzzyText

from hw.flask_factory.models import Client, Parking, db


class ClientFactory(SQLAlchemyModelFactory):
    class Meta:
        model = Client
        sqlalchemy_session = db.session

    name = Faker("first_name")
    surname = Faker("last_name")
    credit_card = FuzzyText(length=12, chars=string.digits)
    car_number = FuzzyText(length=10, chars=string.digits + string.ascii_uppercase)


class ParkingFactory(SQLAlchemyModelFactory):
    class Meta:
        model = Parking
        sqlalchemy_session = db.session

    address = Faker("address")
    opened = LazyAttribute(lambda o: choice((False, True)))
    cost_per_hour = LazyAttribute(lambda o: float(randint(1, 100)))
    count_places = LazyAttribute(lambda o: randint(5, 20))
    count_available_places = LazyAttribute(
        lambda o: o.count_places - randint(1, o.count_places - 1)
    )
