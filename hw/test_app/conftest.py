import pytest

from hw.flask_factory import create_app
from hw.flask_factory.models import BarrierOpening, Client, Parking, db
from hw.flask_factory.queries import add_models


@pytest.fixture(scope="module")
def app():
    app = create_app(
        {"TESTING": True, "SQLALCHEMY_DATABASE_URI": "sqlite:///test_parking.db"}
    )
    with app.app_context():
        session = db.session
        db.drop_all()
        db.create_all()
        parking = Parking(
            count_places=50, count_available_places=50, cost_per_hour=15, opened=True
        )
        barrier = BarrierOpening(parking_id=1)
        client = Client(
            name="Alisa",
            surname="Bogomolova",
        )
        add_models(session, (parking, client, barrier))
        # check_in(session, parking.id, client.id)
        # check_out(session, parking.id, client.id)

        yield app
        # db.drop_all()


@pytest.fixture()
def client(app):
    with app.test_client() as c:
        yield c


@pytest.fixture()
def session(app):
    with app.app_context():
        yield db.session
