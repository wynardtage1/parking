import logging

from flask_factory import create_app


def build_exception_collector():
    exc_collector = logging.getLogger("exception_collector")
    exc_collector.setLevel(logging.INFO)
    handler = logging.FileHandler("unhandled_exceptions.txt")
    handler.setFormatter(
        logging.Formatter(
            "Module: %(module)s | Func: %(funcName)s | Line: %(lineno)s | %(message)s\n\n"
        )
    )
    exc_collector.addHandler(handler)


if __name__ == "__main__":
    build_exception_collector()
    create_app().run()
